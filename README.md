# Project Demos

Please watch the project demonstrations for some of my R&D projects at the following links:

**1. Automatic Power Pole Inspection**==>[Video 1](https://youtu.be/-R-mP9HIM3Q)==>[Video 2](https://youtu.be/SLHeW05xFkg)==>[Report Link](https://drive.google.com/file/d/1S70fVpEAj-lms0siZlgMLzleuN0C6p71/view?usp=sharing)<br/>
**2. Automatic Shark Detection**==>[Video 1](https://www.youtube.com/watch?v=L_htPepvnjk&t=50s)<br/>
**3. Botvision**==>[Video 1](https://youtu.be/oM1llyRpR9U)==>[Video 2](https://youtu.be/QJGBOxqJNhQ)<br/>
**4. Drone Detection**==>[Video 1](https://youtu.be/6K5KwlI4Oas)==>[Video 2](https://youtu.be/wyK8Gkk74iQ)==>[Video 3](https://youtu.be/JhBv1mwLR1o)<br/>
**5. Codebots**==>[Video 1](https://youtu.be/k5bHAw_D1YU)<br/>
**6. Crowd Counting**==>[Video 1](https://youtu.be/Z1wdZKUeTFU)<br/>
**7. Cattle Detection**==>[Video 1](https://youtu.be/6_FTRVbLuV8)<br/>
**8. Swimmer and Surfer Counter**==>[Video 1](https://youtu.be/TD0rDLDrknM)<br/>


# NOTE: These videos are solely for the purpose of showcasing my skills. Please don't download and distribute anywhere.
